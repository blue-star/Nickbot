var http = require('http');
var fs = require('fs');

if (!Object.entries) {
    Object.entries = function( obj ){
      var ownProps = Object.keys( obj ),
          i = ownProps.length,
          resArray = new Array(i); // preallocate the Array!
      while (i--)
        resArray[i] = [ownProps[i], obj[ownProps[i]]];
      
      return resArray;
    };
  }

exports.admins = 
[
    "124136556578603009" //Timfa
]

exports.cmds = 
[
    {
        cmd: "help",
        params: "category",
        category: "main",
        execute: function(bot, info, args)
        {
            var cmds = ["Commands:"];
            var wip = ["Work In Progress (may not work correctly or at all):"]
            
            var categories = ["Categories:"];

            var category = args.join(" ").toLowerCase();

            if(args.length == 0)
            {
                for(var i = 0; i < bot.commands.length; i++)
                {
                    var found = false;

                    for(var c = 0; c < categories.length; c++)
                    {
                        if(categories[c] == "- " + bot.commands[i].category || bot.commands[i].hidden == true || bot.commands[i].category == "wip")
                        {
                            found = true;
                        }
                    }

                    if(!found)
                    {
                        categories.push("- " + bot.commands[i].category);
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Help Categories: ```" + categories.join("\n") /*+ (wip.length > 1? "\n\n" + wip.join("\n") : "")*/ + "```",
                    typing: false
                });
            }
            else
            {
                for(var i = 0; i < bot.commands.length; i++)
                {
                    if(!bot.commands[i].hidden && bot.commands[i].category == category)
                    {
                        var lines = "";

                        for(var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                            lines += "-";

                        if(!bot.commands[i].wip)
                            cmds.push (">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                        else
                            wip.push (">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Commands: ```" + cmds.join("\n") /*+ (wip.length > 1? "\n\n" + wip.join("\n") : "")*/ + "```",
                    typing: false
                });
            }
        }
    },
    {
        cmd: "debughelp",
        params: "none",
        hidden: true,
        category: "admin",
        execute: function(bot, info, args)
        {
            var cmds = ["Commands:"];
            var hidden = ["Admin-only commands:"];
            
            for(var i = 0; i < bot.commands.length; i++)
            {
                var lines = "";

                for(var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                    lines += "-";

                if(!bot.commands[i].hidden)
                    cmds.push (">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                else
                    hidden.push (">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
            }

            bot.sendMessage({
                to: info.channelID,
                message: "Admins: <@" + exports.admins.join("> <@") + "> ```" + /*cmds.join("\n") + "\n\n" + */ hidden.join("\n") + "```",
                typing: false
            });
        }
    },
    {
        cmd: "serverid",
        params: "none",
        hidden: true,
        category: "admin",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "Server ID: `" + bot.data[info.serverId].serverId + "`",
                typing: false
            });
        }
    },
    {
        cmd: "invitelink",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,//511040
                message: "https://discordapp.com/oauth2/authorize?&client_id=479575060345257986&scope=bot&permissions=511040",
                typing: false
            });
        }
    },
    {
        cmd: "ping",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "<@!" + info.userID + ">" + ' Pong!',
                typing: false
            });
        }
    }, 
    {
        cmd:"update",
        params: "none",
        hidden:true,
        category: "admin",
        execute:function(bot, info, args)
        {
            var exec = require('child_process').exec;

            bot.data.update = info.channelID;

            bot.sendMessage({
                to: info.channelID,
                message: "Fetching changes...",
                typing: false
            }, function()
            {
                exec('git fetch --all', function(err, stdout, stderr) 
                {
                    exec('git log --oneline master..origin/master', function(err, stdout, stderr) 
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: (stdout == ""? "No changes, " : "Changes: ```" + stdout + "``` Updating and ") + "reloading!",
                            typing: false
                        }, function()
                        {
                            bot.suicide();
                        });
                    });
                });
            });
        }
    },
    {
        cmd: "echo",
        params: "anything",
        category: "admin",
        hidden: true,
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: args.join(" "),
                typing: true
            });
        }
    },
    {
        cmd: "rawecho",
        params: "anything",
        category: "admin",
        hidden: true,
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "```" + args.join(" ") + "```",
                typing: true
            });
        }
    },
    {
        cmd: "memdump",
        params: "none",
        hidden: true,
        category: "admin",
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "```" + JSON.stringify(bot.data[info.serverId]) + "```",
                typing: false
            });
        }
    },
    {
        cmd: "globalmemdump",
        params: "none",
        hidden: true,
        category: "admin",
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "```" + JSON.stringify(bot.data) + "```",
                typing: false
            });
        }
    },
    {
        cmd: "clearlocalmem",
        params: "none",
        hidden: true,
        category: "admin",
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "Clearing memory!",
                typing: false
            });

            bot.data[info.serverId] = {};
        }
    },
    {
        cmd: "clearglobalmem",
        params: "none (untested)",
        hidden: true,
        category: "admin",
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "Clearing global memory!",
                typing: false
            });

            bot.data = {};
        }
    },
    {
        cmd: "play",
        params: "'playing' string",
        category: "meta",
        execute:function(bot, info, args)
        {
            bot.setPresence({game:
                {
                    name:args.join(" "),
                    type: 0
                }
            });
            
            bot.sendMessage({
                to: info.channelID,
                message: "_Now playing " + args.join(" ") + "_",
                typing:false
            })
        }
    },
    {
        cmd: "stream",
        params: "'streaming' string",
        category: "meta",
        execute:function(bot, info, args)
        {
            bot.setPresence({game:
                {
                    name:args.join(" "),
                    type: 1
                }
            });
            
            bot.sendMessage({
                to: info.channelID,
                message: "_Now streaming " + args.join(" ") + "_",
                typing:false
            })
        }
    },
    {
        cmd: "listento",
        params: "'listening to' string",
        category: "meta",
        execute:function(bot, info, args)
        {
            bot.setPresence({game:
                {
                    name:args.join(" "),
                    type: 2
                }
            });
            
            bot.sendMessage({
                to: info.channelID,
                message: "_Now listening to " + args.join(" ") + "_",
                typing:false
            })
        }
    },
    {
        cmd: "watch",
        params: "'watching' string",
        category: "meta",
        execute:function(bot, info, args)
        {
            bot.setPresence({game:
                {
                    name:args.join(" "),
                    type: 3
                }
            });

            bot.sendMessage({
                to: info.channelID,
                message: "_Now watching " + args.join(" ") + "_",
                typing:false
            })
        }
    },
    {
        cmd: "forcebackup",
        params: "nothing",
        category: "admin",
        hidden: true,
        execute:function(bot, info, args)
        {
            fs.writeFile("./botDataBackup.json", JSON.stringify(bot.data), function(err) {
                if (err) {
                    console.log(err);
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Failed to create a back-up.",
                        typing: false
                    });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Bot data back-up created.",
                        typing: false
                    });
                }
            });
        }
    },
    {
        cmd: "givefile",
        params: "nothing",
        category: "admin",
        hidden: true,
        execute:function(bot, info, args)
        {
            var url = args.join(" ");

            bot.uploadFile({
                to: info.channelID,
                file: url
            }, function(error, response)
            {
                console.log(error);
            })
        }
    },
    {
        cmd: "topmentions",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            var mentions = Object.entries(bot.data[info.serverId].mentions);

            mentions.sort(function(a, b){return b[1] - a[1]});

            if(mentions.length > 10)
                mentions.splice(10, mentions.length - 10);

            var str = "Top 10 Mentioned Users: \n";

            for(var i = 0; i < mentions.length; i++)
            {
                str += (i + 1) + ": " + (i < 9? " " : "") + "<@!" + mentions[i][0] + "> at " + mentions[i][1] + (mentions[i][1] == 1? " mention!\n" : " mentions!\n");
            }

            bot.sendMessage({
                to: info.channelID,
                message: str,
                typing:false
            });
        }
    },
    {
        cmd: "lastreload",
        params: "none",
        hidden: true,
        category: "admin",
        execute: function(bot, info, args)
        {
            fs.readFile('./lastreboot.txt', function read(err, data) {
                if (err) {
                    data = "Unknown";
                }
                
                bot.sendMessage({
                    to: info.channelID,
                    message: data,
                    typing:false
                });
            });
        }
    },
    {
        cmd: "logdump",
        params: "nothing",
        hidden: true,
        category: "admin",
        execute:function(bot, info, args)
        {
            var url = "plog.txt";
            //exec("type plog.txt > tlog.txt;type log.txt >> tlog.txt", function(err, stdout, stderr) 
            //{
                bot.uploadFile({
                    to: info.channelID,
                    file: "crashlog.txt"
                }, function(error, response)
                {
                    bot.uploadFile({
                        to: info.channelID,
                        file: "currentlog.txt"
                    }, function(error, response)
                    {
                        console.log(error);
                    })
                })
            //})
        }
    },
    {
        cmd: "exec",
        params: "command",
        hidden: true,
        category: "admin",
        execute: function(bot, info, args)
        {
            var command = args.join(" ");

            var exec = require('child_process').exec
            exec(command, function(err, stdout, stderr) 
            {
                console.log(stdout)
                bot.sendMessage({
                    to: info.channelID,
                    message: "```" + stdout + "```",
                    typing:false
                })
            })
        }
    },
    {
        cmd: "version",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            var exec = require('child_process').exec
            exec('git log --oneline -n 1 HEAD', function(err, stdout, stderr) 
            {
                console.log(stdout)
                bot.sendMessage({
                    to: info.channelID,
                    message: "Current version: ```" + stdout + "```",
                    typing:false
                })
            })
        }
    }
]

function rand(min, max)
{
    return min + (Math.floor(Math.random() *(max - min)));
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}